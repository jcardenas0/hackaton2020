# -*- coding: utf-8 -*-

import sys
sys.path.append('./processing')

from processing.imports import *
from processing.data_preparation import *
from processing.training import *
from processing.predict import *

    

CANDIDATES_DATA_FILE = '../DataHackaton/Candidates.csv'
VACANTS_FILE = '../DataHackaton/Vacants.csv'
STAGE_FILE = '../DataHackaton/Stages.csv'
APPLICATION_FILE = '../DataHackaton/Applications.csv'
APPLICATIONSTAGE_FILE = '../DataHackaton/ApplicationStages.csv'


def data_setting(vacantsPath, applicationsPath, candidatesPath, stagePath, appStagePath):
    #True significa que están en entrenamiento
    data_preparation_run(True,vacantsPath, applicationsPath, candidatesPath, stagePath, appStagePath)
 
def fit():
    trainModel()

def predict_match(vacantsPredPath, applicationsPredPath, candidatesPredPath, stagePredPath, appStagePredPath):
    predict_pipeline(vacantsPredPath, applicationsPredPath, candidatesPredPath, stagePredPath, appStagePredPath)
    
if __name__ == "__main__": 
    print("loading modules...")