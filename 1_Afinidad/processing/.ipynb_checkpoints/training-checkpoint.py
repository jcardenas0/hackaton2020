# -*- coding: utf-8 -*-
import sys
import os

sys.path.append('./processing')


from processing.imports import *
from processing.preprocessing import *
from processing.utils import *
from processing.featureExtraction import *


try:
    import tensorflow
except ImportError:
    print("Trying to Install required module: TensorFlow\n")
    os.system('python3 -m pip install tensorflow')

import tensorflow as tf

try:
    import keras
except ImportError:
    print("Trying to Install required module: keras\n")
    os.system('python3 -m pip install keras')

from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import np_utils
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import seaborn



    
from keras import optimizers
from keras import initializers
from keras.utils import to_categorical
from keras.callbacks import EarlyStopping
from sklearn.utils import shuffle
from sklearn import svm
from sklearn import metrics

from joblib import dump, load

from matplotlib import pyplot



def loadtrainData():
    global datasetFeatures
    datasetFeatures = loadFeatures('dataML.csv')
    print(datasetFeatures.describe())
    datasetFeatures = shuffle(datasetFeatures)

    #df_corr = datasetFeatures.corr()
    #plt.figure(figsize=(15,10))
    #seaborn.heatmap(df_corr)
    #seaborn.set(font_scale=2)
    #plt.title('Heatmap correlation')
    #plt.show()
    
def setFeaturesandLabels():
    
    global datasetFeatures,normalized_features,labels_cat,labels

   #datasetFeatures = tf.keras.utils.normalize(datasetFeatures, axis=-1, order=2)
    #columna con la variable de salida
    labels = datasetFeatures['progress'].reset_index(drop=True)
    labels = pd.get_dummies(data=labels, columns=['progress'], prefix=["progress"])
    print(labels)
    features = datasetFeatures.drop(['progress'], axis=1).reset_index(drop=True)

    #features = features.drop(['index','status_deleted','status_discarded','vacant_id','no_studies_True','no_experience_True','candidate_id','id',"gender_empty","gender_other_gender","gender_unknown","no_experience_False","status_active","no_experience_False","no_studies_False"], axis=1)
    features = features.drop(['index','vacant_id','candidate_id','id','status_deleted','status_discarded','vacant_id',"gender_empty","gender_other_gender","gender_unknown"], axis=1)

    features = 100*features.astype(float)


    #normalized_features = tf.keras.utils.normalize(features, axis=-1, order=2)
   # from sklearn.preprocessing import MinMaxScaler
   # from joblib import dump, load

    #scalar = MinMaxScaler()
    #scalar.fit(features)

    #normalized_features = scalar.transform(features)
    #dump(scalar, 'scaler.gz')

    #from scipy.sparse import csr_matrix
    #new=csr_matrix(normalized_features)
    
    from sklearn.preprocessing import Normalizer
    from joblib import dump, load

    normalizer = Normalizer()
    normalizer.fit(features)

    normalized_features = normalizer.transform(features)

    dump(normalizer, 'scaler.gz')



def fitModel():
    
    global model,datasetFeatures,normalized_features,labels_cat, X_train, X_test, y_train, y_test

    from keras.regularizers import l2


    X_train, X_test, y_train, y_test = train_test_split(normalized_features, labels, test_size=0.2, random_state=1) 


    model = Sequential()
    opt = optimizers.Adam(lr=0.000001)

    initializer = initializers.RandomNormal(mean=0.0, stddev=0.5, seed=None)

    model.add(Dense(10,  input_dim=normalized_features.shape[1], activation="tanh"))
    model.add(Dense(units=50, activation="tanh", kernel_initializer=initializer, bias_initializer="ones"))
    model.add(Dense(units=5, activation="tanh", kernel_initializer=initializer, bias_initializer="ones"))
    model.add(Dense(2, activation='sigmoid'))


    model.compile(loss='mean_squared_error', optimizer=opt, metrics=['accuracy'])                   
    es = EarlyStopping(monitor='val_loss', mode='min', verbose=0, patience=15)
    history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=4, batch_size=6,verbose=1,callbacks=[es])

    model.save_weights('model.h5')

    model_json = model.to_json()
    with open("model.json", "w") as json_file:
        json_file.write(model_json)


                 
def evaluateModel():
    # evaluate the model

    _,train_mse = model.evaluate(X_train, y_train, verbose=0)
    _,test_mse = model.evaluate(X_test, y_test, verbose=0)

    print('Train: %.3f, Test: %.3f' % (train_mse, test_mse))
    
def fitSVMModel():

    global datasetFeatures,normalized_features,labels_cat, X_train, X_test, y_train, y_test


    X_train, X_test, y_train, y_test = train_test_split(normalized_features, labels, test_size=0.2, random_state=1) 

    #Create a svm Classifier
    modelSVM = svm.SVC(kernel='linear', verbose=True) # Linear Kernel

    #Train the model using the training sets
    modelSVM.fit(X_train, y_train)

    #Predict the response for test dataset
    y_pred = modelSVM.predict(X_test)

    # Model Accuracy: how often is the classifier correct?
    print("Accuracy:",metrics.accuracy_score(y_test, y_pred))
    dump(modelSVM, 'SVMmodel.joblib') 

def trainModel(): 
    
    loadtrainData()
    setFeaturesandLabels()
    fitModel()
    evaluateModel()
    #fitSVMModel()