import numpy as np
import pandas as pd
import csv
import os
from sklearn import datasets
from pandas import DataFrame, HDFStore
from pandas import ExcelWriter
from pandas import ExcelFile
from slugify import Slugify
import warnings
import tables
import xlrd


os.getcwd()


def write_unsafe_attrs() :
    original_warnings = list(warnings.filters)
    warnings.simplefilter('ignore', tables.NaturalNameWarning)


def read_CSV(name, columns_names, ChunkSize):
    
    global df_array,num_columns, num_rows

    df_array = pd.read_csv(name, sep=",")
    df_array = format_column_names(df_array,columns_names)

    
    return [df_array];


def loadFeatures(name):
    
    df_array = pd.read_csv(name)
    
    return df_array;

def create_dataStructure(name):

    global store
    store = HDFStore(name, mode= 'a')
    return store

def convert_to_hdf_name(dataSet,name):

    store.append(name, dataSet, data_columns=True,min_itemsize=50)

def close_dataset():
    store.close()
    

def format_column_names(data_to_format, column_names):

    data_to_format.columns = column_names
    return data_to_format


def getExcelFiles(dataset,fileName,numberofRows):
    
    dataset.describe(include="all")
    dataset[:numberofRows].to_excel(fileName+".xlsx")

#*********************    
    
def CombineColumns(arrayName,dataSet):

    for i in enumerate(dataSet[arrayName[0]]): 
        j = dataSet[arrayName[1]][i[0]] 
        # printing the third element of the column 
        if i[1]!='' and j!='':
            dataSet[arrayName[0]].iloc[i[0]] =  i[1]+ "/" +j
        else:
            dataSet[arrayName[0]].iloc[i[0]] =  i[1]+j
    
    
    return dataSet

def ReplaceValuesColumns(arrayNames,dataSet):
    keyArray=[]
    for i in arrayNames: 
        keyArray.append(i)
        dataSet[i] = dataSet[i].replace('1', arrayNames[i])
    
      
    return dataSet

def addColValues(arrayNames,dataSet):
    keyArray=[]
    string=''
    for i in arrayNames: 
        keyArray.append(i)

    for j in range(len(keyArray)-1, 0 , -1):
        dataSet[keyArray[0]] =  dataSet[keyArray[0]] + dataSet[keyArray[j]]   
   
    return dataSet

def dropCols(arrayNames,dataSet):
    keyArray=[]
    for i in arrayNames: 
        keyArray.append(i)

    for j in keyArray:
        dataSet.drop(j, axis=1, inplace=True)   
   
    return dataSet


def createColsfromCellValues(dataset, colname):
    custom_slugify = Slugify(to_lower=True)
    custom_slugify.separator = '_'

    uniquevalues = list(filter(lambda x : x != '', dataset[colname].unique()))  
    uniquevaluesCleaned =[]
    for loc in uniquevalues:
        loc = str(loc)   #here 'nan' is converted to a string to compare with if
        if loc != 'nan' and loc != '1' :
            uniquevaluesCleaned.append(loc)

    for item in uniquevaluesCleaned:
        if type(item) != np.nan:
            dataset[custom_slugify(item.replace('\r', ''))] = dataset[colname].apply(lambda x: np.all(x==item))