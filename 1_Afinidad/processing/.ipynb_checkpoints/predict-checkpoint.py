# -*- coding: utf-8 -*




import sys
import os

sys.path.append('./processing')


from processing.imports import *
from processing.preprocessing import *
from processing.utils import *
from processing.featureExtraction import *
from processing.data_preparation import *
from processing.training import *

try:
    import tensorflow
except ImportError:
    print("Trying to Install required module: TensorFlow\n")
    os.system('python3 -m pip install tensorflow')

import tensorflow as tf

try:
    import keras
except ImportError:
    print("Trying to Install required module: keras\n")
    os.system('python3 -m pip install keras')

from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import np_utils
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import seaborn
from sklearn.preprocessing import LabelEncoder


from keras.models import model_from_json
from keras import optimizers
from keras import initializers
from keras.utils import to_categorical
from keras.callbacks import EarlyStopping
from sklearn.utils import shuffle
from sklearn import svm
from sklearn import metrics

from joblib import dump, load

from matplotlib import pyplot



def loadDataToPredict():
    global datasetFeatures

    datasetFeatures = loadFeatures('dataToPredict.csv')
    dataML = loadFeatures('dataML.csv')
    colsFull =  dataML.columns
    colsPred =  datasetFeatures.columns


    #las columnas pueden ser menos al aplicar 
    missingCols = np.setdiff1d(colsFull,colsPred)

    datasetFeatures[missingCols] = pd.DataFrame([[0] * len(missingCols)], index=datasetFeatures.index)

    #mezclando los datos para romper la secuencialidad
    datasetFeatures = shuffle(datasetFeatures)

    #ordenar las columnas alfabeticamente
    datasetFeatures = datasetFeatures.reindex(sorted(datasetFeatures.columns), axis=1)
 
    
def setFeaturesandLabels():
    
    global datasetFeatures,normalized_features,labels_cat,labels

    
    labels = datasetFeatures['progress'].reset_index(drop=True)
    features = datasetFeatures.drop(['progress'], axis=1).reset_index(drop=True)
    features = features.drop(['index','vacant_id','candidate_id','id','status_deleted','status_discarded','vacant_id',"gender_empty","gender_other_gender","gender_unknown"], axis=1)

    features = 100*features.astype(float)


    from sklearn.preprocessing import MinMaxScaler
    from joblib import dump, load


    scalar = load('scaler.gz')

    normalized_features = scalar.transform(features)

    from scipy.sparse import csr_matrix
    new=csr_matrix(normalized_features)
    print(new.shape)

    
def predict():
    
    global model,normalized_features,labels_cat,labels

   
    ## load json and create model
    json_file = open('model.json', 'r')

    loaded_model_json = json_file.read()
    json_file.close()

    model = model_from_json(loaded_model_json)
    model.load_weights('model.h5')

    # Predict on the first 5 test images.
    predictions = model.predict(normalized_features)


    # Print our model's predictions.
    for i in range(len(predictions)):
        string= "label category: "+str(labels[i])+" Label predictions: "+ str((predictions[i]))
        print(string)
    # Check our predictions against the ground truths.

def predictSVMModel():

    global datasetFeatures,normalized_features


    modelSVM = load('SVMmodel.joblib') 

    #Predict the response for test dataset
    y_pred = modelSVM.predict(normalized_features)
       
    for i in range(len(labels_cat)):
        string= "label category: "+str(labels_cat[i])+" Label predictions: "+ str(y_pred[i])
        print(string)
    


def predict_pipeline(vacantsPredPath, applicationsPredPath, candidatesPredPath, stagePredPath, appStagePredPath): 
    
    #makes de same preparation for data as the training process
    #output file dataToPredict.csv
    
    data_preparation_run(False,vacantsPredPath, applicationsPredPath, candidatesPredPath, stagePredPath, appStagePredPath)
    
    loadDataToPredict()
    setFeaturesandLabels()
    predict()
    #predictSVMModel()