# -*- coding: utf-8 -*-
from .utils import write_unsafe_attrs,read_CSV,close_dataset,dropCols,getExcelFiles,convert_to_hdf_name,create_dataStructure

from .imports import *

def init_dataset(vacantsPath, applicationsPath, candidatesPath, stagePath, appStagePath):
        
    global dfLabels, dfDB, CandidatesDataSet,VacantsDataSet, StagesDataSet, ApplicationsDataSet, ApplicationStagesDataSet

    
    [CandidatesDataSet] = read_CSV(candidatesPath,CANDIDATE_COLUMNS, CHUNKSIZE)
    print(CandidatesDataSet.columns)
    [VacantsDataSet] = read_CSV(vacantsPath,VACANTS_COLUMNS,CHUNKSIZE)
    [StagesDataSet] = read_CSV(stagePath,STAGE_COLUMNS,CHUNKSIZE)
    [ApplicationsDataSet] = read_CSV(applicationsPath,APPLICATION_COLUMNS,CHUNKSIZE)
    [ApplicationStagesDataSet] = read_CSV(appStagePath,APPLICATIONSTAGE_COLUMNS,CHUNKSIZE)
    
    return [CandidatesDataSet,VacantsDataSet,StagesDataSet,ApplicationsDataSet,ApplicationStagesDataSet]

    
def cleanData():
    global dfLabels, dfDB, CandidatesDataSet,VacantsDataSet, StagesDataSet, ApplicationsDataSet, ApplicationStagesDataSet

    ApplicationsDataSet['progress'] = np.nan
    ApplicationsDataSet['profile_relation'] = np.nan
    ApplicationsDataSet['experience_relation'] = np.nan
    ApplicationsDataSet['titles_relation'] = np.nan
    ApplicationsDataSet['gender'] = np.nan
    ApplicationsDataSet['without_experience'] = np.nan
    ApplicationsDataSet['without_studies'] = np.nan
    ApplicationsDataSet['required_education_relation'] = np.nan

    CandidatesDataSet = CandidatesDataSet.fillna(np.nan)
    VacantsDataSet = VacantsDataSet.fillna(np.nan)
    StagesDataSet = StagesDataSet.fillna(np.nan)
    ApplicationsDataSet = ApplicationsDataSet.fillna(np.nan)
    ApplicationStagesDataSet = ApplicationStagesDataSet.fillna(np.nan)
    
    
    CandidatesDataSet = dropCols(["email","first_name","last_name","phone", "identification_type","identification_number", "has_video","psy_tests"],CandidatesDataSet)
    VacantsDataSet = dropCols(["created_at","company", "publish_date","confidential","agree", "expiration_date"],VacantsDataSet)
    StagesDataSet = dropCols(["send_sms","send_email","send_call", "stage_order"],StagesDataSet)
    ApplicationsDataSet = dropCols(["discarded_type","created_at"],ApplicationsDataSet)
    ApplicationStagesDataSet = dropCols(["status","created_at"],ApplicationStagesDataSet)
    
    return [CandidatesDataSet,VacantsDataSet,StagesDataSet,ApplicationsDataSet,ApplicationStagesDataSet]

    
def cleanVacants():
    global VacantsDataSet

    #remove all status=0 vacants
    VacantsDataSet.drop(VacantsDataSet.loc[VacantsDataSet['status']==0].index, inplace=True)

    #get all vacant id's that have stages. I will remove any other vacant that has no stage or activity related.
    valid_vacants = StagesDataSet['vacant_id'].unique()

    #remove from Vacants all the rows for vacants without stages
    #We need to have only the vacants and applications that have 
    #de TRANSITIONS between POSTULANTE=0/ATRACCION=1 and other stages.

    VacantsDataSet = VacantsDataSet.loc[VacantsDataSet['id'].isin(valid_vacants)]
    
    return VacantsDataSet
    
def transitionExtraction():

    ##add progress column to application stages in order to check which application ar changing state.
    
    global ApplicationStagesDataSet
    
    ApplicationStagesDataSet["progress"] = np.nan

    for i in range(8):
        index_list = []

        #Lista de indices es 'Stages' cuyo 'stage_type' es postulante=0, atraccion=1 etc. etc... 
        index_list = StagesDataSet[StagesDataSet['stage_type']==i].id.values.tolist()

        #extraigo de Application Stages, los indices de los datos cuyos "stage_id " coinciden con 
        #la lista anterior

        section= ApplicationStagesDataSet[['stage_id']]
        appstageIndexes = section.index[section['stage_id'].isin(index_list)]

        #Itero sobre application Stages, llenando con ceros las aplicaciones que aparecen con 
        #postulante y/o atraccion
        #lleno con 1 aquellas aplicaciones que estan en otros estados.

        if i == 0 or i == 1:
            ApplicationStagesDataSet.iloc[appstageIndexes, ApplicationStagesDataSet.columns.get_loc("progress")] = 0
        else:
            ApplicationStagesDataSet.iloc[appstageIndexes, ApplicationStagesDataSet.columns.get_loc("progress")] = 1

            
##########-----------------------

    #seleccionar las aplicaciones que si cambiaron de estado. que progresaron.
    positiveApplications = ApplicationStagesDataSet[ApplicationStagesDataSet['progress']==1].copy()

    #seleccionar aquellas que aparecen que no progresaron. esto incluye las que progresaron posteriormente pero pasaron por estados
    #postulante=0, atraccion=1
    negApplications = ApplicationStagesDataSet[ApplicationStagesDataSet['progress']==0].copy()

    #removemos las que tienen application_id duplicado. Es posible que hayan pasado por postulante
    #y luego por atracción y otros estados. 
    #solo queremos conservar aquellas aplicaciones que EFECTIVAMENTE NUNCA PROGRESARON
    
    
    #***********OJO CON ESTO, VOLVER A ORGANIZARLO
    negApplications.drop_duplicates('application_id', keep='first', inplace=True)
    posIndexes = positiveApplications['application_id'].tolist()
    negToRemove = negApplications.index[negApplications['application_id'].isin(posIndexes)]
    negApplications.drop(negToRemove, inplace=True)

    #unimos los dos arreglos en uno solo que será el 
    #nuevo ApplicationStages

    frames = [positiveApplications, negApplications]
    result = pd.concat(frames)

    #este contiene solo las aplicaciones que progresaron y las que no, sin ningun tipo de repetición.
    ApplicationStagesDataSet = result.sort_index()
    
    return ApplicationStagesDataSet


def datasetTextCleaning():
    
    global VacantsDataSet, ApplicationsDataSet,CandidatesDataSet
    TAG_RE = re.compile(r'<[^>]+>')
    
    #actual ids in Vacants. Some have been removed because STATUS=0
    ids = VacantsDataSet['id'].values.tolist()

    #remove from applications all rows that are not included in Vacants.
    ApplicationsDataSet = ApplicationsDataSet[ApplicationsDataSet['vacant_id'].isin(ids)]


    VacantsDataSet['description']  = [TAG_RE.sub('', text) for text in VacantsDataSet['description'].astype(str)]
    CandidatesDataSet['studies']  = [TAG_RE.sub('', text) for text in CandidatesDataSet['studies'].astype(str)]
    CandidatesDataSet['experiences']  = [TAG_RE.sub('', text) for text in CandidatesDataSet['experiences'].astype(str)]

    return [VacantsDataSet, ApplicationsDataSet,CandidatesDataSet]
    
def saveData(ds, filename):
    global store

    #store =  create_dataStructure('dataML.h5')
    #convert_to_hdf_name(ds,'dataML.h5')
    ds.to_csv (rf"{filename}", header=True, index=False)
    #close_dataset()
