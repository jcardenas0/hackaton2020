# -*- coding: utf-8 -*-

from .utils import write_unsafe_attrs,read_CSV,close_dataset,dropCols,getExcelFiles,convert_to_hdf_name,create_dataStructure

from .imports import *

def textComparison(string1, string2):
    
    stemmer = SnowballStemmer('spanish')
    

    try:
        profiles = string1
    except:
        profiles = ""

    try:
        description = string2
    except:
        description=""

    #text cleaning
    X_words = profiles.lower()
    Y_words = description.lower()

    X_words = X_words.translate( string.punctuation)
    Y_words = Y_words.translate( string.punctuation)

    # tokenization 
    X_words = word_tokenize(X_words)  
    Y_words = word_tokenize(Y_words) 


    # sw contains the list of stopwords 
    sw = stopwords.words('spanish')  

    # remove stop words from string 
    X_set = {w for w in X_words if not w in sw}  
    Y_set = {w for w in Y_words if not w in sw}
    
    #stemm words. "extraer las raices"
    stemmed1 =[];stemmed2 =[] 

    for item in X_set:
        stemmed1.append(stemmer.stem(item))

    for item in Y_set:
        stemmed2.append(stemmer.stem(item))

    # form a set containing keywords of both strings  
    #bag of words
    rvector = X_set.union(Y_set)  
    l1 =[];l2 =[] 

    for w in rvector: 
        if w in X_set: l1.append(1) # create a vector 
        else: l1.append(0) 
        if w in Y_set: l2.append(1) 
        else: l2.append(0) 

    c = 0

    # This part
    #calculates la proporcion de palabras similares
    #que hay entre dos string
    #me arroja un coeficiente del nivel de similitud entre ambos textos.
    
    for i in range(len(rvector)): 
            c+= l1[i]*l2[i] 
            
    if sum(l1)==0 or sum(l2)==0:
        cosine = 0
    else:
        cosine = c / float((sum(l1)*sum(l2))**0.5) 
    
    return cosine


def datasetFeaturePrepartion(vacantDS,candidateDS, applicationDS, appstagesDS):
    
    
    vacantDS = vacantDS[['id','title', 'description', 'education_level','requirements','titles_and_studies', 'experience_and_positions']]



    #Preparing Vacants 
    #We need to remove all NAN from those colums.
    vacantsCopy = vacantDS.copy()
    #vacantsCopy = vacantsCopy[vacantsCopy['description'].notna()]
    #vacantsCopy = vacantsCopy[vacantsCopy['title'].notna()]
    #vacantsCopy = vacantsCopy[vacantsCopy['experience_and_positions'].notna()]
    vacantsCopy = vacantsCopy.fillna(value="empty")
    vacantsCopy = vacantsCopy.replace("[]", "empty") 


    candidatesCopy = candidateDS.copy()

    #candidatesCopy = candidatesCopy[candidatesCopy['profile_description'].notna()]
    candidatesCopy = candidatesCopy.fillna(value="empty")
    candidatesCopy = candidatesCopy.replace("[]", "empty") 

    #Create a clean version of the applications list.
    applicationDS = applicationDS.fillna(np.nan)

    #get all id's of the applications.
    applicationId = applicationDS['id'].tolist()

    #filter applications stages with applications ID
    #so we remove all other applications stages that are not currently included in application dataset
    appstagesDS = appstagesDS.loc[appstagesDS['application_id'].isin(applicationId)]

    
    #WE are going to create the table with features to train the NN
    #la tabla será la misma de applications
    #but with new columns including gender and the relation
    #between vacants and candidates.

    features = applicationDS.copy()
    featuresDataSet = features.reset_index(inplace=False) 

    #BASE BATCH select just a small part of the whole table
    #other wise is going to take days to extract and combine data

    featuresDataSet = featuresDataSet.tail(n=BASE_BATCH)
    #featuresDataSet = featuresDataSet.iloc[3500:BASE_BATCH+3500]

    print(featuresDataSet.shape[0])

    if int(featuresDataSet.shape[0]/BASE_BATCH) < 1 :
    
        FeaturesChuncks = np.array_split(featuresDataSet, 1 )  
    else:
        FeaturesChuncks = np.array_split(featuresDataSet, int(featuresDataSet.shape[0]/BASE_BATCH))

        
    return [FeaturesChuncks,vacantsCopy,candidatesCopy,appstagesDS]

def prepareCategorical(dataframe):
    
    featuresDummies = pd.get_dummies(data=dataframe, columns=['gender',"status",'without_experience','without_studies'], prefix=["gender","status", "no_experience", "no_studies"])

    #dataframe = pd.concat([dataframe, featuresDummies],axis=1)
    
    return featuresDummies.dropna()
    