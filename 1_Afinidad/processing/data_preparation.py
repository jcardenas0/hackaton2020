# -*- coding: utf-8 -*-

import sys
sys.path.append('./processing')

from .imports import *
from .preprocessing import *
from .utils import *
from .featureExtraction import *
from .data_preparation import *
import time

global counter,dfLabels, dfDB, CandidatesDataSet,VacantsDataSet, StagesDataSet, ApplicationsDataSet, ApplicationStagesDataSet


def importDataset(vacantsPath, applicationsPath, candidatesPath, stagePath, appStagePath):
    
    global CandidatesDataSet,VacantsDataSet,StagesDataSet,ApplicationsDataSet,ApplicationStagesDataSet
    updt(100, 5)
    
    [CandidatesDataSet,VacantsDataSet,StagesDataSet,ApplicationsDataSet,ApplicationStagesDataSet] = init_dataset(vacantsPath, applicationsPath, candidatesPath, stagePath, appStagePath)
    
    [CandidatesDataSet,VacantsDataSet,StagesDataSet,ApplicationsDataSet,ApplicationStagesDataSet] = cleanData()
    VacantsDataSet = cleanVacants()
    
def transitionExtract():
    global CandidatesDataSet,VacantsDataSet,StagesDataSet,ApplicationsDataSet,ApplicationStagesDataSet
    updt(100, 10)

    ApplicationStagesDataSet = transitionExtraction()
    print(ApplicationStagesDataSet.head(2))

def initialCleaning():
    global CandidatesDataSet,VacantsDataSet,StagesDataSet,ApplicationsDataSet,ApplicationStagesDataSet
    updt(100, 20)

    [VacantsDataSet, ApplicationsDataSet,CandidatesDataSet] = datasetTextCleaning()

def featurePrep():
    
    global vacantsCopy,candidatesCopy,FeaturesChuncks,CandidatesDataSet,VacantsDataSet,StagesDataSet,ApplicationsDataSet,ApplicationStagesDataSet
    updt(100, 30)

    [FeaturesChuncks,vacantsCopy,candidatesCopy,ApplicationStagesDataSet] = datasetFeaturePrepartion(VacantsDataSet,CandidatesDataSet, ApplicationsDataSet, ApplicationStagesDataSet)

def getFeaturesTable(FeaturesChuncks):

    global counter
    
    pool = mp.Pool(processes = (mp.cpu_count() - 1))
    chunk_list = []
    for chunk in FeaturesChuncks:  
        

        df_split = np.array_split(chunk, num_partitions)
        df_partial = pd.concat(pool.map(fillfeaturesData,df_split))
        chunk_list.append(df_partial)
        df_partial = []

    df_concat = pd.concat(chunk_list)

    pool.close()
    pool.join()
    
    return df_concat



def fillfeaturesData(dataset):
    global counter, CandidatesDataSet,VacantsDataSet,StagesDataSet,ApplicationsDataSet,ApplicationStagesDataSet

    counter= counter+1
    updt(100, 30+counter)
    
    def fillData(x):

        appliStage = ApplicationStagesDataSet[ApplicationStagesDataSet['application_id']==x['id']]
        
        if not appliStage.empty:


        
            x['progress'] = appliStage.progress.values[0]
            vacantRow = vacantsCopy[vacantsCopy.id==x['vacant_id']]
            candidateRow = candidatesCopy[candidatesCopy.id==x['candidate_id']]
        
            if not vacantRow.empty and not candidateRow.empty:
                
                
                x['without_studies'] = candidateRow['without_studies'].astype(str).values[0]
                x['without_experience'] = candidateRow['without_experience'].astype(str).values[0]
                x["profile_relation"] = textComparison(vacantRow['description'].astype(str).values[0]+vacantRow['title'].astype(str).values[0], candidateRow['profile_description'].astype(str).values[0])
                x["titles_relation"] = textComparison(vacantRow['titles_and_studies'].astype(str).values[0], candidateRow['studies'].astype(str).values[0])
                x["experience_relation"] = textComparison(vacantRow['experience_and_positions'].astype(str).values[0],candidateRow['experiences'].astype(str).values[0])
                x["gender"] = candidateRow['gender'].astype(str).values[0]
                x["required_education_relation"] = textComparison(vacantRow['education_level'].astype(str).values[0], candidateRow['education_level'].astype(str).values[0])

            else:

                if not vacantRow.empty:

                    x['without_studies'] = 'False'
                    x['without_experience'] = 'False'
                    x["profile_relation"] = textComparison("empty", vacantRow['description'].astype(str).values[0]+vacantRow['title'].astype(str).values[0])
                    x["titles_relation"] = textComparison("empty", vacantRow['titles_and_studies'].astype(str).values[0])
                    x["experience_relation"] = textComparison("empty", vacantRow['experience_and_positions'].astype(str).values[0])
                    x["required_education_relation"] = textComparison(vacantRow['education_level'].astype(str).values[0], "empty")

                    x["gender"] = "empty"
                    
                if not candidateRow.empty:

                    
                    x['without_studies'] = candidateRow['without_studies'].astype(str).values[0]
                    x['without_experience'] = candidateRow['without_experience'].astype(str).values[0]
                    x["profile_relation"] = textComparison(candidateRow['profile_description'].astype(str).values[0], "empty")
                    x["titles_relation"] = textComparison(candidateRow['studies'].astype(str).values[0], "empty")
                    x["experience_relation"] = textComparison(candidateRow['experiences'].astype(str).values[0], "empty")
                    x["required_education_relation"] = textComparison("empty", candidateRow['education_level'].astype(str).values[0])

                    x["gender"] = candidateRow['gender'].astype(str).values[0]
        return x
    

    dataset = dataset.apply(fillData, axis=1)
    return dataset

def updt(total, progress):
    """
    Displays or updates a console progress bar.

    Original source: https://stackoverflow.com/a/15860757/1391441
    """
    barLength, status = 20, ""
    progress = float(progress) / float(total)
    if progress >= 1.:
        progress, status = 1, "\r\n"
    block = int(round(barLength * progress))
    text = "\r[{}] {:.0f}% {}\n".format(
        "#" * block + "-" * (barLength - block), round(progress * 100, 0),
        status)
    sys.stdout.write(text)
    sys.stdout.flush()
  
    
def data_preparation_run(trainingFlag,vacantsPath, applicationsPath, candidatesPath, stagePath, appStagePath):
    global counter
    counter = 0
    
    updt(100, 1)
    importDataset(vacantsPath, applicationsPath, candidatesPath, stagePath, appStagePath)
    transitionExtract()
    initialCleaning()
    featurePrep()
    filledFeaturesTable = getFeaturesTable(FeaturesChuncks)
    datasetFeatures = filledFeaturesTable.copy()
    updt(100, 70)
    datasetFeatures = prepareCategorical(datasetFeatures)
    updt(100, 90)
    print(datasetFeatures.tail(1))
    if trainingFlag:
        saveData(datasetFeatures, 'dataML.csv')
    else:
        saveData(datasetFeatures, 'dataToPredict.csv')
        

    updt(100, 100)
