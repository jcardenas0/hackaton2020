# -*- coding: utf-8 -*-

from .utils import write_unsafe_attrs,read_CSV,close_dataset,dropCols,getExcelFiles,convert_to_hdf_name,create_dataStructure

from .imports import *

def textComparison(string1, string2):
    
    stemmer = SnowballStemmer('spanish')
    

    try:
        profiles = string1
    except:
        profiles = ""

    try:
        description = string2
    except:
        description=""

    #text cleaning
    X_words = profiles.lower()
    Y_words = description.lower()

    X_words = X_words.translate( string.punctuation)
    Y_words = Y_words.translate( string.punctuation)

    # tokenization 
    X_words = word_tokenize(X_words)  
    Y_words = word_tokenize(Y_words) 


    # sw contains the list of stopwords 
    sw = stopwords.words('spanish')  

    # remove stop words from string 
    X_set = {w for w in X_words if not w in sw}  
    Y_set = {w for w in Y_words if not w in sw}
    
    #stemm words. "extraer las raices"
    stemmed1 =[];stemmed2 =[] 

    for item in X_set:
        stemmed1.append(stemmer.stem(item))

    for item in Y_set:
        stemmed2.append(stemmer.stem(item))

    # form a set containing keywords of both strings  
    #bag of words
    rvector = X_set.union(Y_set)  
    l1 =[];l2 =[] 

    for w in rvector: 
        if w in X_set: l1.append(1) # create a vector 
        else: l1.append(0) 
        if w in Y_set: l2.append(1) 
        else: l2.append(0) 

    c = 0

    # This part
    #calculates la proporcion de palabras similares
    #que hay entre dos string
    #me arroja un coeficiente del nivel de similitud entre ambos textos.
    
    for i in range(len(rvector)): 
            c+= l1[i]*l2[i] 
            
    if sum(l1)==0 or sum(l2)==0:
        cosine = 0
    else:
        cosine = c / float((sum(l1)*sum(l2))**0.5) 
    
    return cosine


def datasetFeaturePrepartion(psyDS,candidateDS):
    


    #Preparing Vacants 
    #We need to remove all NAN from those colums.

   # psyDS['duracion_vinculacion'] = psyDS[psyDS['duracion_vinculacion'].notna()]

    #psyDS['RESPC'] = psyDS[psyDS['RESPC'].notna()]
    #psyDS['RESPE'] = psyDS[psyDS['RESPE'].notna()]
    #psyDS['RESPF'] = psyDS[psyDS['RESPF'].notna()]
    #psyDS['RESPH'] = psyDS[psyDS['RESPH'].notna()]
    #psyDS['RESPQ'] = psyDS[psyDS['RESPQ'].notna()]
    #psyDS['RESPS'] = psyDS[psyDS['RESPS'].notna()]
    #psyDS['ESCALAA'] = psyDS[psyDS['ESCALAA'].notna()]
    #psyDS['ESCALAC'] = psyDS[psyDS['ESCALAC'].notna()]
    #psyDS['ESCALAE'] = psyDS[psyDS['ESCALAE'].notna()]
    #psyDS['ESCALAF'] = psyDS[psyDS['ESCALAF'].notna()]
    #psyDS['ESCALAH'] = psyDS[psyDS['ESCALAH'].notna()]
    #psyDS['ESCALAQ'] = psyDS[psyDS['ESCALAQ'].notna()]
    #psyDS['ESCALAS'] = psyDS[psyDS['ESCALAS'].notna()]



    #featuresDataSet = psyDS.reset_index(inplace=False) 

    #BASE BATCH select just a small part of the whole table
    #other wise is going to take days to extract and combine data

    featuresDataSet = psyDS.tail(n=BASE_BATCH)
    #featuresDataSet = featuresDataSet.iloc[3500:BASE_BATCH+3500]

    if int(featuresDataSet.shape[0]/BASE_BATCH) < 1 :
    
        FeaturesChuncks = np.array_split(featuresDataSet, 1 )  
    else:
        FeaturesChuncks = np.array_split(featuresDataSet, int(featuresDataSet.shape[0]/BASE_BATCH))


    return [FeaturesChuncks,psyDS]

def prepareCategorical(featuresDataSet, arrayCols):
    
    
    featuresDummies = pd.get_dummies(data=featuresDataSet, columns=arrayCols, prefix=arrayCols)

    #dataframe = pd.concat([dataframe, featuresDummies],axis=1)
    
    return featuresDummies
    