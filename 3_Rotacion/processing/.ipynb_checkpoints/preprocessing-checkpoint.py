# -*- coding: utf-8 -*-
from .utils import *
from .imports import *
import datetime

def init_dataset(psytestPath, candidatesPath):
        
    global dfLabels, dfDB, CandidatesDataSet,PsyDataSet

    
    [CandidatesDataSet] = read_CSV_NoHeader(candidatesPath,CANDIDATE_COLUMNS, CHUNKSIZE)
    [PsyDataSet] = read_CSV_NoHeader(psytestPath,PSY_COLUMNS,CHUNKSIZE)
   
    
    return [CandidatesDataSet,PsyDataSet]

    
def cleanData():
    global dfLabels, dfDB, CandidatesDataSet,PsyDataSet

    CandidatesDataSet = CandidatesDataSet.fillna(np.nan)
    PsyDataSet = PsyDataSet.fillna(np.nan)

    
    CandidatesDataSet = dropCols(["email","first_name","last_name","phone", "identification_type", "has_video","psy_tests"],CandidatesDataSet)
    
    PsyDataSet = dropCols(["Causales_de_Retiro", "Motivos_de_Retiros"],PsyDataSet)
    
    
    
    return [CandidatesDataSet,PsyDataSet]

    

    
def featureExtraction():

    ##add progress column to application stages in order to check which application ar changing state.
    
    global PsyDataSet
    
    PsyDataSet["duracion_vinculacion"] = np.nan
    PsyDataSet["rotacion"] = np.nan
  
    PsyDataSet['Fecha_retiro'] = PsyDataSet['Fecha_retiro'].fillna(datetime.date.today())    
    PsyDataSet['Fecha_ingreso'] = PsyDataSet['Fecha_ingreso'].fillna(datetime.date.today())

    PsyDataSet['Fecha_ingreso']= pd.to_datetime(PsyDataSet['Fecha_ingreso'], errors='coerce')    
    PsyDataSet['Fecha_retiro']= pd.to_datetime(PsyDataSet['Fecha_retiro'], errors='coerce') 
    
    
    PsyDataSet["duracion_vinculacion"] = PsyDataSet['Fecha_retiro'] - PsyDataSet['Fecha_ingreso']
    PsyDataSet["duracion_vinculacion"] = PsyDataSet["duracion_vinculacion"].dt.days
    

    
    return PsyDataSet


    
def saveData(ds, filename):
    global store

    #store =  create_dataStructure('dataML.h5')
    #convert_to_hdf_name(ds,'dataML.h5')
    ds.to_csv (rf"{filename}", header=True, index=False)
    #close_dataset()
