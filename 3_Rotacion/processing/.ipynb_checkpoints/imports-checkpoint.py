import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import networkx as nx
import os
import h5py
import yaml
import csv
import json
import collections
import sklearn
import re

import multiprocessing as mp


#from preprocessing import 


from tables import *
from slugify import slugify, Slugify
from bs4 import BeautifulSoup

#****NLP imports

import nltk
from nltk.stem import SnowballStemmer
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 
import string




os.getcwd()

dfLabelsnew = []
dfDB=pd.DataFrame(),
CandidatesDataSet=pd.DataFrame()
VacantsDataSet=pd.DataFrame()
StagesDataSet=pd.DataFrame()
ApplicationsDataSet=pd.DataFrame()
ApplicationStagesDataSet=pd.DataFrame()

features=pd.DataFrame()

BASE_BATCH = 100000
CHUNKSIZE = 50
num_partitions = 5

CANDIDATE_COLUMNS = ['id','email',
                     'first_name','last_name',
                     'phone','brithdate',
                     'gender','identification_type',
                     'identification_number','country_bird','city',
                     'education_level','salary', 'profile_description',
                     'without_experience','without_studies',
                    'title_or_profession','available_to_move',
                    'civil_status','has_video','studies','experiences','psy_tests']

PSY_COLUMNS = ['Identificacion','Fecha_ingreso',
                     'Fecha_retiro','Tipo_de_contrato',
                     'Clasificacion','Causales_de_Retiro',
                     'Motivos_de_Retiros','RESPA','ESCALAA',
                     'RESPC','ESCALAC','RESPE','ESCALAE','RESPF','ESCALAF',
                     'RESPH','ESCALAH','RESPQ','ESCALAQ', 'RESPS','ESCALAS','ITPC']



