# -*- coding: utf-8 -*




import sys
import os

sys.path.append('./processing')


from processing.imports import *
from processing.preprocessing import *
from processing.utils import *
from processing.featureExtraction import *
from processing.data_preparation import *
from processing.training import *

try:
    import tensorflow
except ImportError:
    print("Trying to Install required module: TensorFlow\n")
    os.system('python3 -m pip install tensorflow')

import tensorflow as tf

try:
    import keras
except ImportError:
    print("Trying to Install required module: keras\n")
    os.system('python3 -m pip install keras')

from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import np_utils
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import Normalizer
from keras.models import model_from_json
from keras import optimizers
from keras import initializers
from keras.utils import to_categorical
from keras.callbacks import EarlyStopping
from sklearn.utils import shuffle
from sklearn import svm
from sklearn import metrics
from joblib import dump, load

from matplotlib import pyplot



def loadDataToPredict():
    global datasetFeatures

    datasetFeatures = loadFeatures('dataToPredict.csv')
    dataML = loadFeatures('dataML.csv')
    colsFull =  dataML.columns
    colsPred =  datasetFeatures.columns

    #las columnas pueden ser menos al aplicar 
    missingCols = np.setdiff1d(colsFull,colsPred)

    datasetFeatures[missingCols] = pd.DataFrame([[0] * len(missingCols)], index=datasetFeatures.index)

    #mezclando los datos para romper la secuencialidad
    datasetFeatures = shuffle(datasetFeatures)

    #ordenar las columnas alfabeticamente
    datasetFeatures = datasetFeatures.reindex(sorted(datasetFeatures.columns), axis=1)
 
    
def setFeaturesandLabels():
    
    global datasetFeatures,normalized_features,labels_cat,labels

    
    #datasetFeatures = tf.keras.utils.normalize(datasetFeatures, axis=-1, order=2)
    #columna con la variable de salida
    datasetFeatures.fillna(0)

    
    labels = datasetFeatures['rotacion'].reset_index(drop=True)
    #labels = pd.get_dummies(data=labels, columns='rotacion', prefix="rotacion")
    features = datasetFeatures.drop(['rotacion'], axis=1).reset_index(drop=True)

    #features = features.drop(['index','status_deleted','status_discarded','vacant_id','no_studies_True','no_experience_True','candidate_id','id',"gender_empty","gender_other_gender","gender_unknown","no_experience_False","status_active","no_experience_False","no_studies_False"], axis=1)
    

    features = features.drop(['Identificacion', 
                              'Fecha_ingreso', 'Fecha_retiro','duracion_vinculacion',
                              'Clasificacion_Activo',
                              'Clasificacion_Inactivo','Tipo_de_contrato','Clasificacion',
                              ], axis=1)
    
    #features["duracion_vinculacion"] = features["duracion_vinculacion"].fillna(features["duracion_vinculacion"].mean())

    #features["Clasificacion_Activo"] = features["Clasificacion_Activo"].fillna(features["Clasificacion_Activo"].mean())
    #features["Clasificacion_Inactivo"] = features["Clasificacion_Inactivo"].fillna(features["Clasificacion_Inactivo"].mean())

    features["ESCALAA_DP"] = features["ESCALAA_DP"].fillna(features["ESCALAA_DP"].mean())
    features["ESCALAA_FP"] = features["ESCALAA_FP"].fillna(features["ESCALAA_FP"].mean())
    features["ESCALAA_PS"] = features["ESCALAA_PS"].fillna(features["ESCALAA_PS"].mean())#

    features["ESCALAC_DP"] = features["ESCALAC_DP"].fillna(features["ESCALAC_DP"].mean())
    features["ESCALAC_FP"] = features["ESCALAC_FP"].fillna(features["ESCALAC_FP"].mean())
    features["ESCALAC_PS"] = features["ESCALAC_PS"].fillna(features["ESCALAC_PS"].mean())

    features["ESCALAE_DP"] = features["ESCALAE_DP"].fillna(features["ESCALAE_DP"].mean())
    features["ESCALAE_FP"] = features["ESCALAE_FP"].fillna(features["ESCALAE_FP"].mean())
    features["ESCALAE_PS"] = features["ESCALAE_FP"].fillna(features["ESCALAE_FP"].mean())#

    features["ESCALAF_DP"] = features["ESCALAF_DP"].fillna(features["ESCALAF_DP"].mean())
    features["ESCALAF_FP"] = features["ESCALAF_FP"].fillna(features["ESCALAF_FP"].mean())
    features["ESCALAF_PS"] = features["ESCALAF_PS"].fillna(features["ESCALAF_PS"].mean())
    features["ESCALAH_DP"] = features["ESCALAH_DP"].fillna(features["ESCALAH_DP"].mean())
    features["ESCALAH_FP"] = features["ESCALAH_FP"].fillna(features["ESCALAH_FP"].mean())
    features["ESCALAH_PS"] = features["ESCALAH_PS"].fillna(features["ESCALAH_PS"].mean())

    #features["contrato_Aprendiz"] = features["contrato_Aprendiz"].fillna(features["contrato_Aprendiz"].mean())
    #features["contrato_Termino fijo >= 3 Meses"] = features["contrato_Termino fijo >= 3 Meses"].fillna(features["contrato_Termino fijo >= 3 Meses"].mean())
    #features["contrato_Termino Fijo < 3 Meses"] = features["contrato_Termino Fijo < 3 Meses"].fillna(features["contrato_Termino Fijo < 3 Meses"].mean())
    #features["contrato_Indefinido"] = features["contrato_Indefinido"].fillna(features["contrato_Indefinido"].mean())
    
    features["RESPS"] = features["RESPS"].fillna(features["RESPS"].mean())
    features["RESPQ"] = features["RESPQ"].fillna(features["RESPQ"].mean())
    features["RESPH"] = features["RESPH"].fillna(features["RESPH"].mean())
    features["RESPF"] = features["RESPF"].fillna(features["RESPF"].mean())
    features["RESPE"] = features["RESPE"].fillna(features["RESPE"].mean())
    features["RESPC"] = features["RESPC"].fillna(features["RESPC"].mean())
    features["RESPA"] = features["RESPA"].fillna(features["RESPA"].mean())
    features["RESPE"] = features["RESPE"].fillna(features["RESPE"].mean())

    features["ITPC_PS"] = features["ITPC_PS"].fillna(features["ITPC_PS"].mean())
    features["ITPC_FP"] = features["ITPC_FP"].fillna(features["ITPC_FP"].mean())
    features["ITPC_DP"] = features["ITPC_DP"].fillna(features["ITPC_DP"].mean())

    features["ESCALAS_PS"] = features["ESCALAS_PS"].fillna(features["ESCALAS_PS"].mean())
    features["ESCALAS_FP"] = features["ESCALAS_FP"].fillna(features["ESCALAS_FP"].mean())
    features["ESCALAS_DP"] = features["ESCALAS_DP"].fillna(features["ESCALAS_DP"].mean())

    features["ESCALAQ_PS"] = features["ESCALAQ_PS"].fillna(features["ESCALAQ_PS"].mean())
    features["ESCALAQ_FP"] = features["ESCALAQ_FP"].fillna(features["ESCALAQ_FP"].mean())
    features["ESCALAQ_DP"] = features["ESCALAQ_DP"].fillna(features["ESCALAQ_DP"].mean())

    
    print(features)

    features = features.astype(float)

    
    normalizer = Normalizer()
    normalizer.fit(features)

    normalized_features = normalizer.transform(features)

    dump(normalizer, 'scaler.gz')


    
def predict():
    
    global model,normalized_features,labels_cat,labels

   
    
    # load json and create model
    json_file = open('model.json', 'r')

    loaded_model_json = json_file.read()
    json_file.close()

    model = model_from_json(loaded_model_json)
    model.load_weights('model.h5')

    predictions = model.predict(normalized_features)

    for i in range(len(predictions)):
        string= "label 0: "+str(labels[i])+" Label predictions: "+ str((predictions[i]))
        print(string)

    
def predictSVMModel():

    global datasetFeatures,normalized_features


    modelSVM = load('SVMmodel.joblib') 

    #Predict the response for test dataset
    y_pred = modelSVM.predict(normalized_features)
       
    for i in range(len(labels_cat)):
        string=" Label predictions: "+ str(y_pred[i])
        print(string)
    


def predict_pipeline(psytestPredPath, candidatesPredPath): 
    
    #makes de same preparation for data as the training process
    #output file dataToPredict.csv
    
    data_preparation_run(False,psytestPredPath, candidatesPredPath)
    
    loadDataToPredict()
    setFeaturesandLabels()
    predict()
    #predictSVMModel()