# -*- coding: utf-8 -*-

import sys
sys.path.append('./processing')

from .imports import *
from .preprocessing import *
from .utils import *
from .featureExtraction import *
from .data_preparation import *
import time

global counter,dfLabels, dfDB, CandidatesDataSet,VacantsDataSet, StagesDataSet, ApplicationsDataSet, ApplicationStagesDataSet


def importDataset(psytestPath, candidatesPath):
    
    global PsyDataSet,CandidatesDataSet
    
    updt(100, 5)
    
    [CandidatesDataSet,PsyDataSet] = init_dataset(psytestPath, candidatesPath)
    [CandidatesDataSet,PsyDataSet] = cleanData()
    
   
    
def importDatasetNoHeader(psytestPath, candidatesPath):
    
    global PsyDataSet,CandidatesDataSet
    
    updt(100, 5)
    
    [CandidatesDataSet,PsyDataSet] = init_dataset(psytestPath, candidatesPath)
    [CandidatesDataSet,PsyDataSet] = cleanData()


    
def transitionExtract():
    global CandidatesDataSet,PsyDataSet
    
    updt(100, 10)

    PsyDataSet = featureExtraction()
    

def featurePrep():
    
    global CandidatesDataSet,PsyDataSet,psyCopy,FeaturesChuncks
    
    updt(100, 30)


    [FeaturesChuncks,psyCopy] = datasetFeaturePrepartion(PsyDataSet,CandidatesDataSet)
    
    return [FeaturesChuncks,psyCopy]


def getFeaturesTable(FeaturesChuncks):

    global counter
    
    pool = mp.Pool(processes = (mp.cpu_count() - 1))
    chunk_list = []
    for chunk in FeaturesChuncks:  
        

        df_split = np.array_split(chunk, num_partitions)
        df_partial = pd.concat(pool.map(fillfeaturesData,df_split),sort=True)
        chunk_list.append(df_partial)
        df_partial = []

    df_concat = pd.concat(chunk_list)

    pool.close()
    pool.join()
    
    return df_concat



def fillfeaturesData(dataset):
    global counter

    counter= counter+1
    updt(100, 30+counter)
    
    
    def fillData(x):
        
    
            #si es mayor a 91 dìas
        if x['duracion_vinculacion'] > 90 and x['Clasificacion'] == "Activo":
            x['rotacion'] = 0
        elif x['duracion_vinculacion'] < 90 and x['Clasificacion'] == "Inactivo":

            x['rotacion'] = 1

        elif x['duracion_vinculacion'] > 90 and x['Clasificacion'] == "Inactivo":
            x['rotacion'] = 0
            

        return x
    
    
    dataset = dataset.apply(fillData, axis=1)
    
    dataset = prepareCategorical(dataset,['Tipo_de_contrato',"Clasificacion",'ESCALAA','ESCALAC','ESCALAE','ESCALAF','ESCALAH','ESCALAQ','ESCALAS','ITPC'])
    
    return dataset

def updt(total, progress):
    """
    Displays or updates a console progress bar.

    Original source: https://stackoverflow.com/a/15860757/1391441
    """
    barLength, status = 20, ""
    progress = float(progress) / float(total)
    if progress >= 1.:
        progress, status = 1, "\r\n"
    block = int(round(barLength * progress))
    text = "\r[{}] {:.0f}% {}\n".format(
        "#" * block + "-" * (barLength - block), round(progress * 100, 0),
        status)
    sys.stdout.write(text)
    sys.stdout.flush()
  
    
def data_preparation_run(trainingFlag,psytestPath, candidatesPath):
    global counter,FeaturesChuncks,psyCopy
    
    counter = 0
    
    updt(100, 1)
    
    if trainingFlag:
        
        importDataset(psytestPath, candidatesPath)
    else:
        importDatasetNoHeader(psytestPath, candidatesPath)

    transitionExtract()
    [FeaturesChuncks,psyCopy] = featurePrep()
    
    if trainingFlag:
        filledFeaturesTable = getFeaturesTable(FeaturesChuncks)
    else:
        filledFeaturesTable = psyCopy
        filledFeaturesTable = prepareCategorical(filledFeaturesTable, ['ESCALAA','ESCALAC','ESCALAE','ESCALAF','ESCALAH','ESCALAQ','ESCALAS','ITPC'])
    
    
    updt(100, 70)
    updt(100, 90)
    
    if trainingFlag:

        saveData(filledFeaturesTable, 'dataML.csv')
    else:
        print("Saving DataToPredict")
        saveData(filledFeaturesTable, 'dataToPredict.csv')

    updt(100, 100)
