# -*- coding: utf-8 -*-
import sys
import os

sys.path.append('./processing')


from processing.imports import *
from processing.preprocessing import *
from processing.utils import *
from processing.featureExtraction import *


try:
    import tensorflow
except ImportError:
    print("Trying to Install required module: TensorFlow\n")
    os.system('python3 -m pip install tensorflow')

import tensorflow as tf

try:
    import keras
except ImportError:
    print("Trying to Install required module: keras\n")
    os.system('python3 -m pip install keras')

from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import np_utils
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import seaborn


from keras import optimizers
from keras import initializers
from keras.utils import to_categorical
from keras.callbacks import EarlyStopping
from sklearn.utils import shuffle
from sklearn import svm
from sklearn import metrics

from joblib import dump, load

from matplotlib import pyplot



def loadtrainData():
    global datasetFeatures
    datasetFeatures = loadFeatures('dataML.csv')

    datasetFeatures = shuffle(datasetFeatures)

    #df_corr = datasetFeatures.corr()
    #plt.figure(figsize=(15,10))
    #seaborn.heatmap(df_corr)
    #seaborn.set(font_scale=2)
    #plt.title('Heatmap correlation')
    #plt.show()
    
def setFeaturesandLabels():
    
    global datasetFeatures,normalized_features,labels_cat,labels

   
    #datasetFeatures = tf.keras.utils.normalize(datasetFeatures, axis=-1, order=2)
    #columna con la variable de salida
    datasetFeatures = datasetFeatures[pd.notnull(datasetFeatures['rotacion'])]
    
    labels = datasetFeatures['rotacion'].reset_index(drop=True)

    #labels = pd.get_dummies(data=labels, columns='rotacion', prefix="rotacion")
    features = datasetFeatures.drop(['rotacion'], axis=1).reset_index(drop=True)

    features = features.drop(['Identificacion', 
                              'Fecha_ingreso', 
                'Fecha_retiro',"duracion_vinculacion",
                              "Clasificacion_Activo",
                              "Clasificacion_Inactivo",
                              ], axis=1)

  
    #features[] = features["duracion_vinculacion"].fillna(features["duracion_vinculacion"].mean())

    #features["Clasificacion_Activo"] = features["Clasificacion_Activo"].fillna(features["Clasificacion_Activo"].mean())
    #features["Clasificacion_Inactivo"] = features["Clasificacion_Inactivo"].fillna(features["Clasificacion_Inactivo"].mean())

    features["ESCALAA_DP"] = features["ESCALAA_DP"].fillna(features["ESCALAA_DP"].mean())
    features["ESCALAA_FP"] = features["ESCALAA_FP"].fillna(features["ESCALAA_FP"].mean())
    features["ESCALAA_PS"] = features["ESCALAA_PS"].fillna(features["ESCALAA_PS"].mean())#


    features["ESCALAC_DP"] = features["ESCALAC_DP"].fillna(features["ESCALAC_DP"].mean())
    features["ESCALAC_FP"] = features["ESCALAC_FP"].fillna(features["ESCALAC_FP"].mean())
    features["ESCALAC_PS"] = features["ESCALAC_PS"].fillna(features["ESCALAC_PS"].mean())


    features["ESCALAE_DP"] = features["ESCALAE_DP"].fillna(features["ESCALAE_DP"].mean())
    features["ESCALAE_FP"] = features["ESCALAE_FP"].fillna(features["ESCALAE_FP"].mean())
    features["ESCALAE_PS"] = features["ESCALAE_FP"].fillna(features["ESCALAE_FP"].mean())#


    features["ESCALAF_DP"] = features["ESCALAF_DP"].fillna(features["ESCALAF_DP"].mean())
    features["ESCALAF_FP"] = features["ESCALAF_FP"].fillna(features["ESCALAF_FP"].mean())
    features["ESCALAF_PS"] = features["ESCALAF_PS"].fillna(features["ESCALAF_PS"].mean())
    features["ESCALAH_DP"] = features["ESCALAH_DP"].fillna(features["ESCALAH_DP"].mean())
    features["ESCALAH_FP"] = features["ESCALAH_FP"].fillna(features["ESCALAH_FP"].mean())
    features["ESCALAH_PS"] = features["ESCALAH_PS"].fillna(features["ESCALAH_PS"].mean())


    #features["contrato_Aprendiz"] = features["contrato_Aprendiz"].fillna(features["contrato_Aprendiz"].mean())

    #features["contrato_Termino fijo >= 3 Meses"] = features["contrato_Termino fijo >= 3 Meses"].fillna(features["contrato_Termino fijo >= 3 Meses"].mean())
    #features["contrato_Termino Fijo < 3 Meses"] = features["contrato_Termino Fijo < 3 Meses"].fillna(features["contrato_Termino Fijo < 3 Meses"].mean())
   # features["contrato_Indefinido"] = features["contrato_Indefinido"].fillna(features["contrato_Indefinido"].mean())

    features["RESPS"] = features["RESPS"].fillna(features["RESPS"].mean())
    features["RESPQ"] = features["RESPQ"].fillna(features["RESPQ"].mean())

    features["RESPH"] = features["RESPH"].fillna(features["RESPH"].mean())
    features["RESPF"] = features["RESPF"].fillna(features["RESPF"].mean())
    features["RESPE"] = features["RESPE"].fillna(features["RESPE"].mean())

    features["RESPC"] = features["RESPC"].fillna(features["RESPC"].mean())
    features["RESPA"] = features["RESPA"].fillna(features["RESPA"].mean())
    features["RESPE"] = features["RESPE"].fillna(features["RESPE"].mean())

    features["ITPC_PS"] = features["ITPC_PS"].fillna(features["ITPC_PS"].mean())
    features["ITPC_FP"] = features["ITPC_FP"].fillna(features["ITPC_FP"].mean())
    features["ITPC_DP"] = features["ITPC_DP"].fillna(features["ITPC_DP"].mean())

    features["ESCALAS_PS"] = features["ESCALAS_PS"].fillna(features["ESCALAS_PS"].mean())
    features["ESCALAS_FP"] = features["ESCALAS_FP"].fillna(features["ESCALAS_FP"].mean())
    features["ESCALAS_DP"] = features["ESCALAS_DP"].fillna(features["ESCALAS_DP"].mean())

    features["ESCALAQ_PS"] = features["ESCALAQ_PS"].fillna(features["ESCALAQ_PS"].mean())
    features["ESCALAQ_FP"] = features["ESCALAQ_FP"].fillna(features["ESCALAQ_FP"].mean())
    features["ESCALAQ_DP"] = features["ESCALAQ_DP"].fillna(features["ESCALAQ_DP"].mean())


    features = features.astype(float)


    #normalized_features = tf.keras.utils.normalize(features, axis=-1, order=2)
    from sklearn.preprocessing import Normalizer
    from joblib import dump, load

    normalizer = Normalizer()
    normalizer.fit(features)

    normalized_features = normalizer.transform(features)

    dump(normalizer, 'scaler.gz')




def fitModel():
    
    global model,datasetFeatures,normalized_features,labels_cat, X_train, X_test, y_train, y_test
    
    from keras.regularizers import l2


    X_train, X_test, y_train, y_test = train_test_split(normalized_features, labels, test_size=0.2, random_state=1) 


    model = Sequential()
    opt = optimizers.Adam(lr=0.00001)

    initializer = initializers.RandomNormal(mean=0.0, stddev=0.5, seed=None)

    model.add(Dense(200,  input_dim=normalized_features.shape[1], activation="tanh",kernel_regularizer=l2(0.001)))
    model.add(Dense(units=5, activation="tanh", kernel_initializer=initializer, bias_initializer="ones"))
    model.add(Dense(1, activation='tanh'))

    model.compile(loss='mean_squared_error', optimizer=opt, metrics=['accuracy']) 
    
    es = EarlyStopping(monitor='val_loss', mode='min', verbose=0, patience=15)
    history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=75, batch_size=5,verbose=1,callbacks=[es])

    model.save_weights('model.h5')

    model_json = model.to_json()
    with open("model.json", "w") as json_file:
        json_file.write(model_json)




                 
def evaluateModel():
    # evaluate the model

    __,train_mse = model.evaluate(X_train, y_train, verbose=0)
    _,test_mse = model.evaluate(X_test, y_test, verbose=0)

    print('Train: %.3f, Test: %.3f' % (train_mse, test_mse))
    
def fitSVMModel():

    global datasetFeatures,normalized_features,labels_cat, X_train, X_test, y_train, y_test


    X_train, X_test, y_train, y_test = train_test_split(normalized_features, labels, test_size=0.2, random_state=1) 

    #Create a svm Classifier
    modelSVM = svm.SVC(kernel='linear', verbose=True) # Linear Kernel

    #Train the model using the training sets
    modelSVM.fit(X_train, y_train)

    #Predict the response for test dataset
    y_pred = modelSVM.predict(X_test)

    # Model Accuracy: how often is the classifier correct?
    print("Accuracy:",metrics.accuracy_score(y_test, y_pred))
    dump(modelSVM, 'SVMmodel.joblib') 

def trainModel(): 
    
    loadtrainData()
    setFeaturesandLabels()
    fitModel()
    evaluateModel()
    #fitSVMModel()