# -*- coding: utf-8 -*-

import sys
sys.path.append('./processing')

from processing.imports import *
from processing.data_preparation import *
from processing.training import *
from processing.predict import *

    

CANDIDATES_DATA_FILE = '../DataHackaton/Candidates.csv'
PSY_FILE = '../DataHackaton/Psychometrics.csv'


def data_setting(psytestPath, candidatesPath):
    #True significa que están en entrenamiento
    data_preparation_run(True,psytestPath, candidatesPath)
    
def fit():
    trainModel()

def predict(psytestPredPath, candidatesPredPath):
    predict_pipeline(psytestPredPath, candidatesPredPath)
    
if __name__ == "__main__": 
    print("loading modules...")