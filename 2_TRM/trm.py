# -*- coding: utf-8 -*-

import sys
sys.path.append('./processing')

from processing.imports import *
from processing.data_preparation import *
from processing.predict import *

    


TRM_DATA_FILE = 'DataSeries/1.1.2.TCM_Para rango de fechas dado.csv'
INFLACION_DATA_FILE = 'DataSeries/INF_Serie.csv'
TIB_DATA_FILE = 'DataSeries/TIB_Serie.csv'


def data_setting(trmFilePath,infFilePath,tibFilePath):
    #True significa que están en entrenamiento
    data_preparation_run(True,trmFilePath,infFilePath,tibFilePath)


def predict(trmPredPath,infFilePath,tibFilePath):
    predict_pipeline(trmPredPath,infFilePath,tibFilePath)
    
if __name__ == "__main__": 
    print("loading modules...")