# -*- coding: utf-8 -*-
from .utils import * 
from .imports import *
import datetime

from textwrap import wrap

def init_dataset(trmFilePath,infFilePath,tibFilePath):
        
    global dfLabels, dfDB, TRMDataSet,INFDataSet,TIBDataSet

    
    [TRMDataSet] = read_CSV(trmFilePath,TRM_COLUMNS, CHUNKSIZE)
    
    [INFDataSet] = read_CSV(infFilePath,INFLACION_COLUMNS, CHUNKSIZE)

    [TIBDataSet] = read_CSV(tibFilePath,TASAIB_COLUMNS, CHUNKSIZE)

    
    return [TRMDataSet,INFDataSet,TIBDataSet]

    
def featureExtraction():

    ##add progress column to application stages in order to check which application ar changing state.
    
    global TRMDataSet,INFDataSet,TIBDataSet
    
    
  
    TRMDataSet['Fecha (dd/mm/aaaa)'] = TRMDataSet['Fecha (dd/mm/aaaa)'].fillna(datetime.date.today())    
    TRMDataSet['Fecha (dd/mm/aaaa)']= pd.to_datetime(TRMDataSet['Fecha (dd/mm/aaaa)'], errors='coerce')    
    TRMDataSet.sort_values(by=['Año'], inplace=True, ascending=True)
    TRMDataSet = TRMDataSet[:-1]
    TRMDataSet = TRMDataSet.drop(['Fecha (dd/mm/aaaa)'], axis=1)
    TRMDataSet = TRMDataSet.drop(['MesName'], axis=1)
    TRMDataSet['TRM'] = TRMDataSet['TRM'].str.replace(",","").astype(float)/100
    TRMDataSet['inflacion'] = np.nan
    TRMDataSet['TIB'] = np.nan
    
    TRMDataSet['Año']= TRMDataSet['Año'].astype(int)
    TRMDataSet['dia']= TRMDataSet['dia'].astype(int)
    TRMDataSet['Mes']= TRMDataSet['Mes'].astype(int)

    
    
    TIBDataSet['Fecha'] = TIBDataSet['Fecha'].fillna(datetime.date.today())    
    TIBDataSet['Fecha']= pd.to_datetime(TIBDataSet['Fecha'], errors='coerce')    
    TIBDataSet.sort_values(by=['Fecha'], inplace=True, ascending=True)
    TIBDataSet['TIB']= TIBDataSet['TIB'].str.replace(r'\D', '')  
    TIBDataSet = TIBDataSet[:-1]
    TIBDataSet = TIBDataSet.iloc[1:].reset_index(drop=True)
    TIBDataSet['Year'] = TIBDataSet['Fecha'].dt.year
    TIBDataSet['Month'] = TIBDataSet['Fecha'].dt.month
    TIBDataSet['Day'] = TIBDataSet['Fecha'].dt.day
    TIBDataSet = TIBDataSet.drop(['Fecha'], axis=1)
    TIBDataSet['TIB'] = TIBDataSet['TIB'].dropna()
    TIBDataSet['TIB']= TIBDataSet['TIB'].astype(int)/100

    copyInf = INFDataSet.iloc[1:].copy().reset_index(drop=True)
    copyInf['ano']=np.nan
    copyInf['mes']=np.nan
    
   

    columnIndex = copyInf.columns.get_loc("ano")
    columnIndex2 = copyInf.columns.get_loc("mes")

    for index, row in copyInf.iterrows():
        Series = wrap(str(row[0]), 4)
        copyInf.iloc[index,columnIndex] = Series[0]
        copyInf.iloc[index,columnIndex2] = Series[1]
        
    copyInf.sort_values(by=['ano'], inplace=True, ascending=True)
    copyInf['Variacion anual'] = copyInf['Variacion anual'].str.replace(",","").astype(float)/100   
    copyInf['ano']= copyInf['ano'].astype(int)
    copyInf['mes']= copyInf['mes'].astype(int)
    
    copyInf = copyInf.drop(['DATEString'], axis=1)
    copyInf = copyInf.drop(['Limite inferior'], axis=1)
    copyInf = copyInf.drop(['Meta'], axis=1)
    copyInf = copyInf.drop(['Limite superior'], axis=1)

  

            
    return [TRMDataSet,copyInf,TIBDataSet]

def cleanData():
    
    global dfLabels, dfDB, TRMDataSet,INFDataSet,TIBDataSet

    TRMDataSet = TRMDataSet.fillna(np.nan)
    INFDataSet = INFDataSet.fillna(np.nan)
    TIBDataSet = TIBDataSet.fillna(np.nan)

    TIBDataSet = TIBDataSet[~TIBDataSet['TIB'].isin(['N.A.'])]
    
    return [TRMDataSet,INFDataSet,TIBDataSet]




    
def saveData(ds, filename):
    global store

    #store =  create_dataStructure('dataML.h5')
    #convert_to_hdf_name(ds,'dataML.h5')
    ds.to_csv (rf"{filename}", header=True, index=False)
    #close_dataset()