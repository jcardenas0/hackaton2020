# -*- coding: utf-8 -*




import sys
import os

sys.path.append('./processing')


from processing.imports import *
from processing.preprocessing import *
from processing.utils import *
from processing.featureExtraction import *
from processing.data_preparation import *
from processing.training import *

try:
    import tensorflow
except ImportError:
    print("Trying to Install required module: TensorFlow\n")
    os.system('python3 -m pip install tensorflow')

import tensorflow as tf

try:
    import keras
except ImportError:
    print("Trying to Install required module: keras\n")
    os.system('python3 -m pip install keras')

from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import np_utils
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import seaborn
from sklearn.preprocessing import LabelEncoder


from keras.models import model_from_json
from keras import optimizers
from keras import initializers
from keras.utils import to_categorical
from keras.callbacks import EarlyStopping
from sklearn.utils import shuffle
from sklearn import svm
from sklearn import metrics

from joblib import dump, load


from keras.layers import LSTM
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error




def loadDataToPredict():
    global datasetFeatures

    datasetFeatures = loadFeatures('dataML.csv')
    datasetFeatures.dropna(inplace=True)
   
    import datetime
    

    #Crear time series
    date=datasetFeatures.apply(lambda x: datetime.date(int(x['Año']), int(x['Mes']), int(x['dia'])),axis=1)
    date = pd.to_datetime(date)
    datasetFeatures = datasetFeatures.drop(columns=['Año', 'Mes','dia'])
    datasetFeatures.insert(0, 'Date', date)
    datasetFeatures.sort_values(['Date'], ascending=[False]) 
    datasetFeatures.reset_index(drop=True)
    
    #Time serie= Index del dataset
    #TRM = Y
    #inflacion = X1
    #TIB = X2
    
    
 
    
def setFeaturesandLabels():
    
    global datasetFeatures,normalized_features, Y,reframed, values,scaler
    
    Y = datasetFeatures["TRM"]
    
    datasetFeatures = datasetFeatures.drop('Date', axis=1)
    datasetFeatures = datasetFeatures.drop('TRM', axis=1)


    from sklearn.preprocessing import MinMaxScaler
    
    
    values = datasetFeatures.values
    
    encoder = LabelEncoder()
    values[:,1] = encoder.fit_transform(values[:,1])
    values = values.astype('float32')
   
    # escalar feature
    scaler = MinMaxScaler(feature_range=(0, 1))
    normalized_features = scaler.fit_transform(values)
    
    print(normalized_features)
    
    dump(scaler, 'scaler.gz')
    # del la serie de tiempo de arman los paquete
    #para t-1 y t 
    reframed = series_to_supervised(normalized_features, 1, 1)
    
    print(reframed.head(3))
 

def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
        n_vars = 1 if type(data) is list else data.shape[1]
        df = DataFrame(data)
        cols, names = list(), list()
        # input sequence (t-n, ... t-1)
        for i in range(n_in, 0, -1):
            cols.append(df.shift(i))
            names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
        # forecast sequence (t, t+1, ... t+n)
        for i in range(0, n_out):
            cols.append(df.shift(-i))
            if i == 0:
                names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
            else:
                names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
        # put it all together
        agg = pd.concat(cols, axis=1)
        agg.columns = names
        # drop rows with NaN values
        if dropnan:
            agg.dropna(inplace=True)
        return agg

def fitModel():
    global model,datasetFeatures,reframed,normalized_features,Y, X_train, X_test, y_train, y_test


    # split into train and test sets
    values = reframed.values
    n_train_days = 8000 
    
    train = values[:n_train_days, :]
    test = values[n_train_days:, :]
    
    # split into input and outputs
    X_train, y_train = train[:, :-1], train[:, -1]
    X_test, y_test = test[:, :-1], test[:, -1]
    
    # reshape input to be 3D [samples, timesteps, features]
    X_train = X_train.reshape((X_train.shape[0], 1, X_train.shape[1]))
    
    X_test = X_test.reshape((X_test.shape[0], 1, X_test.shape[1]))
    
  
    model = Sequential()
    model.add(LSTM(50, input_shape=(X_train.shape[1], X_train.shape[2])))
    model.add(Dense(1))
    model.compile(loss='mae', optimizer='adam')
    # fit network
    history = model.fit(X_train, y_train, epochs=50, batch_size=72, validation_data=(X_test, y_test), verbose=1, shuffle=False)

    pyplot.plot(history.history['loss'], label='train')
    pyplot.plot(history.history['val_loss'], label='test')
    pyplot.legend()
    pyplot.show()
    
    model.save_weights('model.h5')

    model_json = model.to_json()
    with open("model.json", "w") as json_file:
        json_file.write(model_json)

    
def predict():
    
    from numpy import concatenate
    from keras.models import load_model

    global  X_test, y_test
    
    # load json and create model
    json_file = open('model.json', 'r')

    loaded_model_json = json_file.read()
    json_file.close()

    model = model_from_json(loaded_model_json)
    model.load_weights('model.h5')

    yhat = model.predict(X_test, verbose=0)
    
    scaler = load('scaler.gz')
 
    print(yhat)
    



def predict_pipeline(trmPredPath,infFilePath,tibFilePath): 
    
    #makes de same preparation for data as the training process
    #output file dataToPredict.csv
    
    data_preparation_run(True,trmFilePath,infFilePath,tibFilePath)
    
    loadDataToPredict()
    setFeaturesandLabels()
    fitModel()
    predict()
