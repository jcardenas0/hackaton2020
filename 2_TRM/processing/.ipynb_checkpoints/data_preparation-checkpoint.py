# -*- coding: utf-8 -*-

import sys
sys.path.append('./processing')

from .imports import *
from .preprocessing import *
from .utils import *
from .featureExtraction import *
from .data_preparation import *
import time

global counter,dfLabels, dfDB, TRMDataSet,INFDataSet,TIBDataSet


def importDataset(trmFilePath,infFilePath,tibFilePath):
    
    global TRMDataSet,INFDataSet,TIBDataSet
    updt(100, 5)
    
    [TRMDataSet,INFDataSet,TIBDataSet] = init_dataset(trmFilePath,infFilePath,tibFilePath)
    
    [TRMDataSet,INFDataSet,TIBDataSet] = cleanData()
    
def transitionExtract():
    global TRMDataSet,INFDataSet,TIBDataSet
    updt(100, 10)

    [TRMDataSet,INFDataSet,TIBDataSet] = featureExtraction()

    print(TRMDataSet.tail(2))
    print(INFDataSet.tail(2))
    print(TIBDataSet.tail(2))

def featurePrep():
    
    global TRMDataSet,FeaturesChuncks,INFDataSet,TIBDataSet
    updt(100, 30)

    [FeaturesChuncks,TRMDataSet,] = datasetFeaturePrepartion(TRMDataSet,INFDataSet,TIBDataSet)

def getFeaturesTable(FeaturesChuncks):

    global counter
    
    pool = mp.Pool(processes = (mp.cpu_count() - 1))
    chunk_list = []
    for chunk in FeaturesChuncks:  
        

        df_split = np.array_split(chunk, num_partitions)
        df_partial = pd.concat(pool.map(fillfeaturesData,df_split))
        chunk_list.append(df_partial)
        df_partial = []

    df_concat = pd.concat(chunk_list)

    pool.close()
    pool.join()
    
    return df_concat



def fillfeaturesData(dataset):
    global counter,INFDataSet,TIBDataSet

    counter= counter+1
    updt(100, 30+counter)
    
    
    def fillData(x):
        
        YearInflation = INFDataSet[INFDataSet['ano']==x['Año']]
        YearTIB = TIBDataSet[TIBDataSet['Year']==x['Año']]
        
        if not YearInflation.empty and not YearTIB.empty :
            
            
            monthInflation = YearInflation[YearInflation['mes']==x['Mes']]
            monthTIB = YearTIB[YearTIB['Month']==x['Mes']]
        
        #**fill Inflation data
            if not monthInflation.empty: 
                                
                x['inflacion'] = monthInflation['Variacion anual'].astype(float).values[0]
            else:
                x['inflacion'] = np.nan
                
        #**fill TBI data

            if not monthTIB.empty: 
                                
                dayTIB = monthTIB[monthTIB['Day']==x['dia']]
                
                if not dayTIB.empty: 
                    x['TIB'] = dayTIB['TIB'].astype(float).values[0]
                else:
                    x['TIB'] = np.nan
                
            else:
                x['inflacion'] = np.nan
                x['TIB'] = np.nan


        else:
            x['inflacion'] = np.nan
            x['TIB'] = np.nan

    
        return x
    
    
    dataset = dataset.apply(fillData, axis=1)
    
    #esta función no requiere categorìas
    ##dataset = prepareCategorical(dataset)
    
    return dataset

def updt(total, progress):
    """
    Displays or updates a console progress bar.

    Original source: https://stackoverflow.com/a/15860757/1391441
    """
    barLength, status = 20, ""
    progress = float(progress) / float(total)
    if progress >= 1.:
        progress, status = 1, "\r\n"
    block = int(round(barLength * progress))
    text = "\r[{}] {:.0f}% {}\n".format(
        "#" * block + "-" * (barLength - block), round(progress * 100, 0),
        status)
    sys.stdout.write(text)
    sys.stdout.flush()
  
    
def data_preparation_run(trainingFlag,trmFilePath,infFilePath,tibFilePath):
    
    global counter,FeaturesChuncks
    
    counter = 0
    
    updt(100, 1)
    
    if trainingFlag:
        
        importDataset(trmFilePath,infFilePath,tibFilePath)
    else:
        importDatasetNoHeader(trmFilePath,infFilePath,tibFilePath)

    transitionExtract()
    featurePrep()

    filledFeaturesTable = getFeaturesTable(FeaturesChuncks)
    
    filledFeaturesTable['TIB'] = filledFeaturesTable['TIB'].fillna((filledFeaturesTable['TIB'].mean()))
    filledFeaturesTable['inflacion'] = filledFeaturesTable['inflacion'].fillna((filledFeaturesTable['inflacion'].mean()))

    print(filledFeaturesTable)
        
    updt(100, 90)
    
    if trainingFlag:

        saveData(filledFeaturesTable, 'dataML.csv')
    else:

        saveData(filledFeaturesTable, 'dataToPredict.csv')

    updt(100, 100)
