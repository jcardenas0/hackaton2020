# -*- coding: utf-8 -*-

from .utils import write_unsafe_attrs,read_CSV,close_dataset,dropCols,getExcelFiles,convert_to_hdf_name,create_dataStructure

from .imports import *

from textwrap import wrap



def datasetFeaturePrepartion(TRMDataSet,INFDataSet,TIBDataSet):
    
    #BASE BATCH select just a small part of the whole table
    #other wise is going to take days to extract and combine data

    
    
    
    
    featuresDataSet = TRMDataSet.tail(n=BASE_BATCH)


    if int(featuresDataSet.shape[0]/BASE_BATCH) < 1 :
    
        FeaturesChuncks = np.array_split(featuresDataSet, 1 )  
    else:
        FeaturesChuncks = np.array_split(featuresDataSet, int(featuresDataSet.shape[0]/BASE_BATCH))

    return [FeaturesChuncks,featuresDataSet]

def prepareCategorical(dataframe):
    
    featuresDummies = pd.get_dummies(data=dataframe, columns=['gender',"status",'without_experience','without_studies'], prefix=["gender","status", "no_experience", "no_studies"])

    #dataframe = pd.concat([dataframe, featuresDummies],axis=1)
    
    return featuresDummies.dropna()
    