import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import networkx as nx
import os
import h5py
import yaml
import csv
import json
import collections
import sklearn
import re

import multiprocessing as mp


#from preprocessing import 


from tables import *
from slugify import slugify, Slugify
from bs4 import BeautifulSoup

#****NLP imports

import nltk
from nltk.stem import SnowballStemmer
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 
import string




os.getcwd()

dfLabelsnew = []
dfDB=pd.DataFrame(),
CandidatesDataSet=pd.DataFrame()
VacantsDataSet=pd.DataFrame()
StagesDataSet=pd.DataFrame()
ApplicationsDataSet=pd.DataFrame()
ApplicationStagesDataSet=pd.DataFrame()

features=pd.DataFrame()

BASE_BATCH = 50000
CHUNKSIZE = 50
num_partitions = 5

TRM_COLUMNS = ['Año','Fecha (dd/mm/aaaa)',
                     'TRM','dia',
                     'MesName','Mes']



INFLACION_COLUMNS = ['DATEString','Variacion anual','Limite inferior','Meta','Limite superior'
              ]

#TASA INTERBANCARIA
TASAIB_COLUMNS = ['Fecha','TIB' 
              ]
